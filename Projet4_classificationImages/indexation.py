import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from pylab import *
from scipy.spatial import distance as dist
from scipy.io import loadmat
import pandas

'''
Affiche l'image choisie aléatoirement et les 5 images qui lui ressemblent
'''
def plotImages(imgNames):
    fig = plt.figure()
    grid = fig.add_gridspec(4, 3)

    plt1 = fig.add_subplot(grid[0, 0])
    plt1.imshow(mpimg.imread(imgNames[0]).astype('uint8'))
    plt1.set_title('Image choisie : '+imgNames[0][13:])

    plt2 = fig.add_subplot(grid[0, 1])
    plt2.imshow(mpimg.imread(imgNames[1]).astype('uint8'))
    plt2.set_title('Image 1 : '+imgNames[1][13:])

    plt3 = fig.add_subplot(grid[0, 2])
    plt3.imshow(mpimg.imread(imgNames[2]).astype('uint8'))
    plt3.set_title('Image 2 : '+imgNames[2][13:])

    plt4 = fig.add_subplot(grid[2, 0])
    plt4.imshow(mpimg.imread(imgNames[3]).astype('uint8'))
    plt4.set_title('Image 3 : '+imgNames[3][13:])

    plt5 = fig.add_subplot(grid[2, 1])
    plt5.imshow(mpimg.imread(imgNames[4]).astype('uint8'))
    plt5.set_title('Image 4 : '+imgNames[4][13:])

    plt6 = fig.add_subplot(grid[2, 2])
    plt6.imshow(mpimg.imread(imgNames[5]).astype('uint8'))
    plt6.set_title('Image 5 : '+imgNames[5][13:])

    plt.show()

'''
Génere un indice aléatoire pour l'image à choisir
'''
def generateRandomIndex(n):
    k = np.random.randint(0,n-1)
    return k

'''
Charge les données caractéristiques des images dans une matrice qui contient les 1000 images en lignes et toutes les
valeurs des 5 mesures concaténées en colonnes. 
'''
def loadData():
    data = {}
    data['phog'] = pandas.read_excel('samples/WangSignatures.xls', header=None, sheet_name=0, index_col=None).values
    data['JCD'] = pandas.read_excel('samples/WangSignatures.xls', header=None, sheet_name=1, index_col=0).values
    data['CEDD'] = pandas.read_excel('samples/WangSignatures.xls', header=None, sheet_name=2, index_col=0).values
    data['FCTH'] = pandas.read_excel('samples/WangSignatures.xls', header=None, sheet_name=3, index_col=0).values
    data['FCH'] = pandas.read_excel('samples/WangSignatures.xls', header=None, sheet_name=4, index_col=0).values
    return np.concatenate((data['phog'],data['JCD'],data['CEDD'], data['FCTH'], data['FCH'] ),axis=1)

# ##################################################   Méthode 1 :  APPROCHE KPPV - selection directe avec Scikit-Learn
'''
Procédure de l'approche de selection directe des images similaires depuis la base données avec scikit-learn
'''
def skLearnDirectKnnAproach():
    # Chargement de l'ensemble de données
    data = loadData();
    # CHoisir une image aléatoire
    randomImage = generateRandomIndex(len(data));
    print(data[randomImage][0]);
    # Calculer les 5 images similaires



# #####################################################        Méthode 2 : APPROCHE KPPV avec calcul de distances

'''
Calcule la distance euclidienne entre deux vecteurs
'''
def  distance(Xi, Wi):
    if len(Xi)!=len(Wi):
        print('ERREUR : tailles des deux vecteurs incompatibles !')
        return -1
    else:
        return dist.euclidean(Xi, Wi)

'''
Calcule les 5 images similaires avec une approche KPPV
'''
def similarImagesKnn(img,data):
    if len(img) != len(data[0]):
        print('ERREUR : tailles des deux vecteurs incompatibles !')
        return -1
    else:
        distances = []
        for i in range(0, len(data)):
            diff = distance(img[1:],data[i, 1:])
            distances.append(diff)
        sortedDistanceArgs = np.argsort(distances)
        image1 = data[sortedDistanceArgs[0]]
        image2 = data[sortedDistanceArgs[1]]
        image3 = data[sortedDistanceArgs[2]]
        image4 = data[sortedDistanceArgs[3]]
        image5 = data[sortedDistanceArgs[4]]
        image6 = data[sortedDistanceArgs[5]]
        # Retourner les coordonnés du cluster gagnant sous forme de vecteur à 2 dimensions.
        return np.array([image1, image2, image3, image4, image5, image6])

'''
Procédure de l'approche KPPV
'''
def knnSelfApproach():
    # Chargement de l'ensemble de données
    data =loadData();
    # CHoisir une image aléatoire
    randomImage = generateRandomIndex(len(data));
    print(data[randomImage][0]);
    # Calculer les 5 images similaires
    print(data[0])
    images = similarImagesKnn(data[randomImage], data);
    # Récupérer les noms des 5 images pour l'affichage
    imageNames = []
    for i in range(len(images)):
        imageNames.append('samples/Wang/'+str(images[i][0]))

    print(imageNames)
    # Affichage
    plotImages(imageNames)

# #########################################################   Méthode BONUS : Similarité cosinus entre deux vecteurs
# Cette méthode n'est pas demandée dans l'énoncé mais qui est interssante car elle donne un bon résultat

'''
Calcule les 5 images similaires avec une approche Similarité cosinus
'''
def similarImagesCos(img,data):
    if len(img) != len(data[0]):
        print('ERREUR : tailles des deux vecteurs incompatibles !')
        return -1
    else:
        cosVector=[]
        for i in range(len(data)):
            cosVector.append(cosTheta(img,data[i]))
        # Trier le tableau de résultats  d'un ordre croissant :
        '''
        INDICATION: cosinus theta est compris dans l'intervalle [-1,1], la valeur -1 indiquera des vecteurs résolument 
        opposés, 0 des vecteurs indépendants (orthogonaux) et 1 des vecteurs similaires, ce qui nous intersese sont les
        derniers éléments du tableau trié qui ont une valeur de cosinus qui tend vers 1.
        '''
        sortedCosVectorArgs = np.argsort(cosVector)
        image1 = data[sortedCosVectorArgs[-1]]
        image2 = data[sortedCosVectorArgs[-2]]
        image3 = data[sortedCosVectorArgs[-3]]
        image4 = data[sortedCosVectorArgs[-4]]
        image5 = data[sortedCosVectorArgs[-5]]
        image6 = data[sortedCosVectorArgs[-6]]
        # Retourner les coordonnés du cluster gagnant sous forme de vecteur à 2 dimensions.
        return np.array([image1, image2, image3, image4, image5, image6])

'''
Calcule le cosinus de l'angle entre deux vecteurs
'''
def cosTheta(x,y):
    if (len(x)!=len(y)):
        print('> cosTheta > Problème de dimension !')
        return None
    else:
        nominateur = np.dot(x[1:],y[1:])
        denominateur = np.linalg.norm(x[1:]) * np.linalg.norm(y[1:])
        return nominateur/denominateur

'''
Procédure de l'approche similarité cosinus
'''
def cosSelfApproach():
    # Chargement de données
    data = loadData();
    # Selection d'une image aléatoirement
    randomImage = generateRandomIndex(len(data));
    print(data[randomImage][0])
    # Calculer les 5 images similaires
    images = similarImagesCos(data[randomImage], data);
    # Récupérer les noms des images pour l'affichage
    imageNames = []
    for i in range(len(images)):
        imageNames.append('samples/Wang/' + str(images[i][0]))

    print(imageNames)
    # Afficher les images
    plotImages(imageNames)


##################################################################   PROGRAMME PRINCIPAL
# knnSelfApproach()
cosSelfApproach()

