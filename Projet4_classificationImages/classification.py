from builtins import print

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import pandas

from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.naive_bayes import GaussianNB

def plotImage(imgIdex, label="" ):
    fig = plt.figure()
    grid = fig.add_gridspec(1, 1)

    plt1 = fig.add_subplot(grid[0, 0])
    imgName = "samples/Wang/"+str(imgIdex)+".jpg"
    plt1.imshow(mpimg.imread(imgName).astype('uint8'))
    plt1.set_title(label)

    plt.show()

def generateRandomIndex(n):
    k = np.random.randint(0,n-1)
    return k

def loadData():
    data = {}
    data['PHOG'] = pandas.read_excel('samples/WangSignatures.xls', header=None, sheet_name=0, index_col=0).values
    data['JCD'] = pandas.read_excel('samples/WangSignatures.xls', header=None, sheet_name=1, index_col=0).values
    data['CEDD'] = pandas.read_excel('samples/WangSignatures.xls', header=None, sheet_name=2, index_col=0).values
    data['FCTH'] = pandas.read_excel('samples/WangSignatures.xls', header=None, sheet_name=3, index_col=0).values
    data['FCH'] = pandas.read_excel('samples/WangSignatures.xls', header=None, sheet_name=4, index_col=0).values
    return np.concatenate((data['PHOG'],data['JCD'],data['CEDD'], data['FCTH'], data['FCH'] ),axis=1)
    #return data['CEDD'];

def generateLabel():
    label = []
    for i in range(0, 1000):
        if i<100 :
            label.append('Jungle')
        elif i <=199:
            label.append('Plage')
        elif i <=299:
            label.append('Monuments')
        elif i <=399:
            label.append('Bus')
        elif i <=499:
            label.append('Dinosaures')
        elif i <=599:
            label.append('Eléphants')
        elif i <=699:
            label.append('Fleurs')
        elif i <=799:
            label.append('Chevaux')
        elif i <=899:
            label.append('Montagne')
        elif i <=999:
            label.append('Plats')
    return label
            
            
def knn(X, y, k_range):
    scores = []
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20, random_state=1)
    for k in range(1, k_range+1):
        randomImageIndex = generateRandomIndex(len(X))
        knn = KNeighborsClassifier(n_neighbors=k)
        knn.fit(X_train, y_train)
        #predictions = knn.predict(X_test)
        scores.append(knn.score(X_test, y_test))

        img = X[randomImageIndex]
        img = img.reshape([1, -1])
        predicted_output = knn.predict(img)
        labelShow = '> KNN (K = '+str(k)+') global score : ' + str(knn.score(X_test, y_test)) + "\n> Random Image :" + str(
            randomImageIndex) + ".png \n> predicted label : " + str(predicted_output[0]) + " \n > actual label :" + str(
            y[randomImageIndex])
        print(labelShow)
        plotImage(randomImageIndex, labelShow)

        cm = confusion_matrix(y_test, knn.predict(X_test))
        print("> KNN (K = "+str(k)+") confusion matrix : \n", cm)
    #print(predictions)
    return scores


def neuralNetwork(X, y):
    randomImageIndex = generateRandomIndex(len(X))
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20, random_state=10)
    clf = MLPClassifier(solver='adam',activation='relu', alpha=0.5, hidden_layer_sizes=(150,100,100), random_state=1)
    clf.fit(X,y)

    img = X[randomImageIndex]
    img = img.reshape([1, -1])
    predicted_output = clf.predict(img)
    labelShow = '> RNN global score : ' + str(clf.score(X_test, y_test)) + "\n> Random Image :" + str(
        randomImageIndex) + ".png \n> predicted label : " + str(predicted_output[0]) + " \n > actual label :" + str(
        y[randomImageIndex])
    print(labelShow)
    score = clf.score(X_test, y_test)
    plotImage(randomImageIndex, labelShow)

    cm = confusion_matrix(y_test, clf.predict(X_test))
    print("> RNN confusion matrix : \n", cm)

    return score

def bayes(X, y):
    randomImageIndex = generateRandomIndex(len(X))
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.48, random_state=1)
    model = GaussianNB()
    model.fit(X_train, y_train)
    img = X[randomImageIndex]
    img = img.reshape([1, -1])
    predicted_output = model.predict(img)
    labelShow = '> Bayes global score : '+str(model.score(X_test, y_test))+"\n> Random Image :"+str(randomImageIndex)+".png \n> predicted label : "+str(predicted_output[0])+" \n > actual label :"+str(y[randomImageIndex])
    print(labelShow)
    score = model.score(X_test, y_test)
    plotImage(randomImageIndex, labelShow )

    cm = confusion_matrix(y_test, model.predict(X_test))
    print("> Bayes confusion matrix : \n",cm)
    return score


def display_scores(k_range, scores):
    plt.plot(k_range, scores)
    plt.xlabel('valeur de K')
    plt.ylabel('la précision')

    plt.show()


# --------------------------------------------------- main program

data = loadData()
labels = generateLabel()

# # applying knn for classification
# k_range = 30
# scores = knn(data, labels, k_range)
# display_scores( range(1,k_range+1), scores)

# # applying Bayes for classification
# scores = bayes(data, labels)

# # applying neural network fro classification
score = neuralNetwork(data, labels)
# print(score)

