##################################################################
#                        PROJET 1
#################################################################

# This file is based on functions
# The functions are defined here on top and called in a main block
# at the bottom


from scipy.io import loadmat
import numpy as np
import matplotlib.pyplot as plt

# ------------------------------------------------------ FUNCTIONS
'''
    aminArg : returns the arg index of minimum value in array
'''
def aminArg(array):
    for i in range(0,len(array)):
        if array[i] == np.amin(array):
            return i
    return -1
'''
    errorRate : calculates the error rate of classes attribution
'''
def errorRate(origClasses, calculatedClasses):
    if len(origClasses) != len(calculatedClasses) :
        print("ERROR : Your output vector of calculated classes has wrong size !")
        return None
    else:
        er = 0
        for i in range(0,len(origClasses)):
            if origClasses[i]!=calculatedClasses[i]:
                er = er+1
        return (er * 100 / len(origClasses))

'''
    getClass: Returns the class of a value within the 150 individual learning set
'''
def getClass(value):
    if value in range (0,50):
        return 1
    elif value in range (50,100):
        return 2
    elif value in range(100, 150):
        return 3
'''
    getClass: Returns the class of a value within the 60 individual learning set
'''
def getClassWithReducedLearningSet(value):
    if value in range (0,20):
        return 1
    elif value in range (20,40):
        return 2
    elif value in range(40, 60):
        return 3
'''
    getClass: Returns the class of a value within the 60 individual learning set
'''
def getClassWithUndefinedDistribution(value):
    if value in range (0,70):
        return 1
    elif value in range (70,140):
        return 2
    elif value in range(140, 210):
        return 3
'''
    getClass: Returns the class of a value within the 450 individual learning set
'''
def getClassWithHugeLearningSet(value):
    if value in range (0,150):
        return 1
    elif value in range (150,300):
        return 2
    elif value in range(300, 450):
        return 3

'''
    getClassWithoutTeacher: Returns the class of a value within the 150 individual 
    learning set having the origin class
'''
def getClassWithoutTeacher(classe_origine, value):
    if  value > len(classe_origine):
        return None
    else:
        return  classe_origine[value]



'''
    distances : Calculates the distance of two values of an individual point to 
    the leaning set points.
    :returns : An array of float distances 
    :param test : The learning set points
    :type test : matrix of 2 rows
    :param x1 : The first value of the individual point
    :type x1 : float
    :param x1 : The second value of the individual point
    :type x1 : float
'''
def distances(test,x1,y1):
    dist = []
    for i in range(0,len(test[0])):
        dist.append(np.sqrt(abs(pow(test[0,i] - x1 , 2)+pow(test[1][i] - y1 , 2))))
    return dist
'''
    kppv : knn algorithm applied to a vector of R² points .
    :returns : An array of int classes 
    :param test : The learning set points
    :type test : matrix of 2 rows
    :param k : Number of neighbours
    :type k : int
    :param x : The vector of individual points in R²
    :type x : array
    :param exercice : The number of the exercice in the subject to excute (optional)
'''
def kppv(test , classe_origine, k , x , exercice = None):
    clas = []
    for i in range(0,len(x[0])):
        # Calculate the distances to learning set
        dist = distances(test,x[0,i],x[1,i])
        # Sort the args of distances array to pick the nearest neighbours
        sortedArgs = np.argsort(dist)
        # Pick the nearest classes along from the k neighbours
        nearestClasses = []
        for j in range(0,k):
            # This test is to see witch exercise we are working on
            if  exercice == 2.1:
                nearestClasses.append(getClass(sortedArgs[j]))
            elif exercice == 2.2:
                nearestClasses.append(getClassWithoutTeacher(classe_origine, sortedArgs[j]))
            elif exercice == 2.3:
                nearestClasses.append(getClassWithReducedLearningSet(sortedArgs[j]))
            elif exercice == 2.4:
                nearestClasses.append(getClassWithHugeLearningSet(sortedArgs[j]))
            elif exercice == 2.5:
                nearestClasses.append(getClassWithUndefinedDistribution(sortedArgs[j]))
        # Pick the best class (most occured value in the array)
        # print(nearestClasses)
        (values, counts) = np.unique(nearestClasses, return_counts=True)
        ind = np.argmax(counts)
        # Get the best class
        bestClass = values[ind]
        clas.append(bestClass)
    return clas

# ########################################################################################################
#                                               MAIN PROGRAM
##########################################################################################################

if __name__ == '__main__':
    ################################################################## Load data samples
    data = loadmat('../samples/p1_data1.mat')
    # print(data.keys())
    test = data['test']
    x = data['x']
    clasap = data['clasapp']
    # Load the origine classes
    if 'orig' in data.keys():
        origine_classes = data['orig'][0]
    else:
        origine_classes = None

    ################################################################      Execute KNN
    # Applying KNN with K=1..15
    k = 15
    errorRates = []
    kValues =[]
    for i in range(1,k+1):
        clas = kppv(test, origine_classes, i,x, 2.1)
        # Error rate and k (used to plot the error rates of k values)
        er = round(errorRate(clasap[0], clas), 2)
        errorRates.append(er)
        kValues.append(i)
        # Log of error rate using k neighbours
        print('Error rate while applying KNN with K = ' + str(i) + ' : ' + str(er) + ' %')
        # Log of the output classes using  k neighbours
        print(clas)
        print('-----------------')

    ################################################################   Prepare the plots
    fig1 = plt.figure()
    plt1 = fig1.add_subplot(111)
    # Plot (graphical diagram) of the error rates
    plt1.plot(kValues, errorRates,'ro--', linewidth=0.5)
    plt1.axis([0, k+2, 0, np.amax(errorRates)+1])
    plt1.set_ylabel('Taux d\'erreur %')
    plt1.set_title('Taux d\'erreur suivant K (KNN | K = 1..'+str(k)+')')
    plt1.set_xlabel('Nombre de voisins K')


    # Get automatically the best k value to recalculate KNN and Plot the result classes
    bestK = aminArg(errorRates)+1

    clas = kppv(test, origine_classes, bestK,x, 2.1)
    x1 = []
    y1 = []
    x2 = []
    y2 = []
    x3 = []
    y3 = []
    for i in range(0,len(x[0])):
        if clas[i] == 1 :
            x1.append(x[0,i])
            y1.append(x[1,i])
        elif clas[i] == 2 :
            x2.append(x[0, i])
            y2.append(x[1, i])
        elif clas[i] == 3 :
            x3.append(x[0, i])
            y3.append(x[1, i])

    # Plot (graphical diagram) the the X individual colored by classes
    fig2 = plt.figure()
    plt2 = fig2.add_subplot(211)
    plt2.scatter(x1, y1, c='red')
    plt2.scatter(x2, y2, c='green')
    plt2.scatter(x3, y3, c='blue')
    plt2.set_title('Nuage de points des individus de X colorés selon leurs classes (KNN | K = '+str(bestK)+')')
    plt2.set_xlabel('X[0,:]')
    plt2.set_ylabel('X[1,:]')

    plt.show()