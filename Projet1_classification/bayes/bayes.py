##################################################################
#                        PROJET 1
#################################################################

# This file is based on functions
# The functions are defined here on top and called in a main block
# at the bottom
import numpy
from scipy.io import loadmat
import numpy as np
import matplotlib.pyplot as plt



# ------------------------------------------------------ FUNCTIONS
'''
Plots the classes
'''
def plotClass(clas, x, error):
    # Points
    ind = (clas == 1)
    plt.plot(x[0, ind], x[1, ind], "o", color="blue")

    ind = (clas == 2)
    plt.plot(x[0, ind], x[1, ind], "o", color="red")

    ind = (clas == 3)
    plt.plot(x[0, ind], x[1, ind], "o", color="green")

    plt.title("Classification avec Bayes, erreur = "+str(error))
    # plot
    plt.show()

'''
Calculates the mean of the learning set classes
'''
def means(learningSet):
    if learningSet is None:
        print('Error calculating the means, learning set is empty !')
        return None
    else:
        means=np.zeros(shape=(2,3))
        # Calculate the var-cov for each class
        lm1 = int(int(len(learningSet[0])) / 3)
        lm2 = 2 * lm1
        lm3 = 3 * lm1
        mean1 = np.mean([learningSet[0][:lm1], learningSet[1][:lm1]])
        mean2 = np.mean([learningSet[0][lm1 - 1:lm2], learningSet[1][lm1 - 1:lm2]])
        mean3 = np.mean([learningSet[0][lm2 - 1:lm3], learningSet[1][lm2 - 1:lm3]])
        # Strore the results in one structure
        means[:, 0] = mean1
        means[:, 1] = mean2
        means[:, 2] = mean3
        return means

'''
Calculates the veriances co-variances of the learning set classes
'''
def sigmas(learningSet):
    sigma=np.zeros(shape=(2,2,3))
    # Calculate the var-cov for each class
    lm1 = int(int(len(learningSet[0])) / 3)
    lm2 = 2 * lm1
    lm3 = 3 * lm1
    sigma1 = np.cov([learningSet[0][:lm1], learningSet[1][:lm1]])
    sigma2 = np.cov([learningSet[0][lm1 - 1:lm2], learningSet[1][lm1 - 1:lm2]])
    sigma3 = np.cov([learningSet[0][lm2 - 1:lm3], learningSet[1][lm2 - 1:lm3]])
    # Strore the results in one structure
    sigma[:, :, 0] = sigma1
    sigma[:,:,1] = sigma2
    sigma[:,:,2] = sigma3

    return np.transpose(sigma)

'''
Probabilities
'''
def probabilities():
    return np.array([1/3 , 1/3 , 1/3])

'''
Naive Bayes function
'''
def bayes(m,sigma,p,x):
    pw_x = np.zeros(3)
    clas = np.zeros(x[0].size)
    for e in range(0, x[0].size):
        for i in range(0, 3):
            diff = np.subtract(x[:, e], m[:,i])
            temp = np.dot(np.transpose(diff), np.linalg.inv(sigma[i]))
            temp1 = np.dot(temp, diff)
            numerator = np.exp((-1 / 2) * temp1)
            denominator = pow(2 * np.pi, p[i]/2) * np.sqrt(np.linalg.det(sigma[i]))
            # Probability of generating instance x given class wm
            fx_w = numerator / denominator
            # Probability a-priori of occurrence of class wm
            pw_m = p[i]
            # Probability of instance x occurring
            fx = 1/len(x[0])
            # Probability of class knowing x
            pw_x[i] = pw_m * fx_w / fx
        # Chose best prob
        ind = np.argsort(pw_x)
        clas[e] = ind[len(ind)-1]+1
    return clas
'''
Calculates error
'''
def errors(clas,clasapp):
    if(len(clas)!= len(clasapp) or len(clas) == 0 ):
        print("ERREUR DE TAILLE")
        return None
    else:
        error = float(0)
        for i in range(0,len(clas)):
            if (clas[i]!=clasapp[i]):
                error+=1
        return error/len(clas)

# ########################################################################################################
#                                               MAIN PROGRAM
##########################################################################################################


################################################################## Load data samples
# You can change here the loaded data sample
data = loadmat('../samples/p1_data4.mat')
print(data.keys())
test = data['test']
x = data['x']
clasapp = data['clasapp']
################################################################      Execute Byes
clas = bayes(means(x), sigmas(x), probabilities(), x)
# print(clas)
################################################################   Plots
plotClass(clas,x, errors(clas,clasapp[0]))