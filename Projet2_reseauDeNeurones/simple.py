import numpy as np
import matplotlib.pyplot as plt
import random

from scipy.io import loadmat

alpha = 0.1


def perceptron(x, w, active):
    s = float(0)
    for i in range(len(x)):
        s = s+x[i]*w[i+1]
    s += w[0]
    if active == 0:
        return int(np.sign(s))
    else:
        return int(np.tanh(s))


def generateWeight():
    weights = np.zeros(3)
    weights[0] = random.randint(-10, 10)
    weights[1] = random.randint(-10, 10)
    weights[2] = random.randint(-10, 100)
    return weights

# Fonction d'apprentissage simple
    # param : x , ensemble d'apprentissage
    # param : yd, résultat attendu
    # param : active, fonction d'activation
def apprentissage_simple(x,yd, active):
    # INITIALISATION : Poids, iteration et erreurs
    w= generateWeight()
    iteration = 1
    errors = list()
    print(w)
    # Boucler sur 100 itérations
    while iteration <= 100:
        error=float(0)
        y = np.ndarray(shape=(len(x[0]), 1))
        for i in range(len(x[0])):
            yi = perceptron(x[:,i], w, active)
            y[i] = yi
            error = error + float(pow(yd[i]-yi, 2))
            r = (yd[i] - yi)
            # Reglage des poids
            w[1] = w[1] + (alpha * r * x[0, i])
            w[2] = w[2] + (alpha * r * x[1, i])
        errors.append(error)

        # Si l'erreur est nulle alors quitter la boucle d'iterations
        if error == 0:
            break

        # Afficher les points classés et la droite séparatrice
        y = np.transpose(y)
        affiche_classe(x, y[0], iteration, w)

        iteration += 1

    return w, errors

def affiche_classe(x, clas, k , w):
    # Points
    ind = (clas == -1)
    plt.plot(x[0, ind], x[1, ind], "o", color = "blue")
    ind = (clas == 1)
    plt.plot(x[0, ind], x[1, ind], "o", color = "red" )

    # Droite séparatrice des classes
    plt.ylim([-10,10])
    plt.xlim([-10,10])
    plt.scatter(x[0], x[1], c='r')
    if k == 0:
        plt.title("Droite séparatrice des poids appliquée sur l'oracle")
    else:
        plt.title("Iteration " + str(k))
    a = -(w[1]/w[2])
    b = -(w[0]/w[2])
    t = [-5,5]
    z = [a * t[0] + b, a * t[1] + b]
    plt.plot(t, z, c="orange")

    # Afficher
    plt.show()

def test1():
    data = loadmat('samples/p2_d1.mat')
    x = data['x']
    oracle = np.concatenate((np.zeros(25) - 1, np.ones(25)))
    w,errors = apprentissage_simple(x, oracle, 1)

    # Evolution de l'erreur
    plt.plot(errors)
    plt.title("Evolution de l'erreur sur les itérations")
    plt.show()

    # Afficher la position de la droite séparatrice par rapport à l'oracle (résultat attendu)
    # C'est pour évluer graphiquement la précision de séparation de cette droite (pondérée par
    # les poids finaux de la dernière itération
    affiche_classe(x, oracle, 0 , w)

def test2():
    data = loadmat('samples/p2_d2.mat')
    x = data['x']
    oracle = np.concatenate((np.zeros(25) - 1, np.ones(25)))
    w,errors = apprentissage_simple(x, oracle, 1)

    # Evolution de l'erreur
    plt.plot(errors)
    plt.title("Evolution de l'erreur sur les itérations")
    plt.show()

    # Afficher la position de la droite séparatrice par rapport à l'oracle (résultat attendu)
    # C'est pour évluer graphiquement la précision de séparation de cette droite (pondérée par
    # les poids finaux de la dernière itération
    affiche_classe(x, oracle, 0 , w)

############################################################################################
#                                   TEST 1
############################################################################################
# Commenter/décommenter cette ligne pour ignorer/effectuer le test1 sur p2_d1.mat
test1()

############################################################################################
#                                   TEST 2
############################################################################################
# Commenter/décommenter cette ligne pour ignorer/effectuer le test1 sur p2_d1.mat
test2()