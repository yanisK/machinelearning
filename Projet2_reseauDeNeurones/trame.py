import numpy as np
import matplotlib.pyplot as plt
import random

from scipy.io import loadmat

alpha = 0.1

def perceptron(x, w, active):
    s = float(0)
    for i in range(len(x)):
        s = s+x[i]*w[i+1]
    s += w[0]
    if active == 0:
        return np.sign(s)
    else:
        return np.tanh(s)

def apprentissage(x, yd, active):
    mdiff = []
    classes = list()
    w = generateWeight(len(x[0]))
    for i in range(100):
        error = float(0)
        for j in range(len(x[0])):
            y = perceptron(x[:, j], w[j], 0)
            print(y)
            classes.append(y)
            error += pow(yd[j] - y, 2)
        for h in range(len(x)):
            for k in range(len(x)):
                w[h][k+1] = w[h][k+1] + alpha*error*x[:,k]
        mdiff.append(error.sum());
    return w, mdiff, classes

def apprentissage_widrow(x, yd):
    mdiff = []
    w = generateWeight(len(x[0]))
    for i in range(100):
        error = float(0)
        for j in range(len(x)):
            y = perceptron(x[:,j], w[j], 0)
            error += pow(yd[j]-y, 2)
            for k in range(len(x)):
                w[j][k+1] = w[j][k+1] - alpha*error*pow((1-y),2)*x[:,k]
        mdiff.append(error.sum());
    return w, mdiff

def generateWeight(numberOfW):
    weights = []
    for i in range(numberOfW):
        w = []
        for i in range(3):
            w.append(random.randint(-1, 3));
        weights.append(w);
    return weights;

def affiche_classe(x, clas, K, w):
    ind = (clas == -1)
    plt.plot(x[0, ind], x[1, ind], "o")
    ind = (clas == 1)
    plt.plot(x[0, ind], x[1, ind], "o")
    plt.scatter(x[0], x[1], c='r')
    a = -(w[1]/w[2])
    b = -(w[0]/w[2])
    t = [-3, 3]
    z = [a * t[0] + b, a * t[1] + b]
    plt.plot(t, z)
    plt.show()

def afficher_droite(x):
    plt.scatter(x[0], x[1], c='r')
    a = -1
    b = 0.5
    t = [-4, 4]
    z = [a * t[0] + b, a * t[1] + b]
    plt.plot(t, z)
    plt.show()


# # Test du Ou Logic
#w = [-0.5, 1, 1];
#print(perceptron([0, 0], w, 0))
#print(perceptron([0, 1], w, 0))
#print(perceptron([1, 0], w, 0))
#print(perceptron([1, 1], w, 0))

#Apprentissage des poids du OU Logic
#x=np.transpose([[0,0],[0,1],[1,0],[1,1]])
#yd=[-1,1,1,1]
#w,mdiff=apprentissage(x,yd,1)
#plt.plot(mdiff)
#plt.show()

#Données de test
Data = loadmat('samples/p2_d1.mat')
x = Data['x']
oracle = np.concatenate((np.zeros(25) - 1, np.ones(25)))
w, mdiff, classes = apprentissage(x, oracle, 0)
plt.plot(mdiff)
plt.show()
affiche_classe(x, oracle, 3, w[0])
#gradient descendant
#Data = loadmat('samples/p2_d1.mat')
#x = Data['x']
#oracle = np.concatenate((np.zeros(25) - 1, np.ones(25)))
#w, mdiff = apprentissage_widrow(x, oracle)
#plt.plot(mdiff)
#plt.show()
#affiche_classe(x, oracle, 2, w)
