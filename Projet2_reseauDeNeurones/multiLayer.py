import random
import numpy as np
import matplotlib.pyplot as plt

# Seuil d'apprentissage
alpha = 0.5

def perceptron(x, weights):
    s = 0;
    for i in range(len(x)):
        s += x[i] * weights[i + 1]
    s += weights[0]
    return 1 / (1 + np.exp(-s))


def multiPerceptron(x, w1, w2):
    y1 = perceptron(x, w1[:,0])
    y2 = perceptron(x, w1[:,1])
    yf = perceptron(np.array([y1, y2]), w2)
    return yf, y1, y2


def multiPerceptron_widrow(x, yd, w1, w2):
    errors = list()
    i = 0
    while i < 5000:
        error = 0.0
        for j in range(len(x[0])):
            yf, y1, y2 = multiPerceptron(x[:, j], w1, w2)
            error = error + pow(yd[j] - yf, 2)
            w2[1] = w2[1] - alpha * (-(yd[j] - yf) * (yf - pow(yf, 2)) * y1)
            w2[2] = w2[2] - alpha * (-(yd[j] - yf) * (yf - pow(yf, 2)) * y2)
            w1[1][0] = w1[1][0] - alpha * (y1 - pow(y1, 2)) * (-(yd[j] - yf) * (yf - pow(yf, 2))) * w2[1] * x[0][j]
            w1[2][0] = w1[2][0] - alpha * (y1 - pow(y1, 2)) * (-(yd[j] - yf) * (yf - pow(yf, 2))) * w2[1] * x[0][j]
            w1[1][1] = w1[1][1] - alpha * (y2 - pow(y2, 2)) * (-(yd[j] - pow(yf, 2)) * (yf - pow(yf, 2))) * w2[2] * x[0][j]
            w1[2][1] = w1[2][1] - alpha * (y2 - pow(y2, 2)) * (-(yd[j] - pow(yf, 2)) * (yf - pow(yf, 2))) * w2[2] * x[0][j]
        errors.append(error)
        i += 1
    return w1, w2, errors


def generateWeight():
    weights = np.zeros(3)
    weights[0] = random.randint(-3, 3)
    weights[1] = random.randint(-3, 3)
    weights[2] = random.randint(-3, 3)
    return weights


def affiche_classe(x, clas, w):
    ind = (clas == 0)
    plt.plot(x[0, ind], x[1, ind], "o")
    ind = (clas == 1)
    plt.plot(x[0, ind], x[1, ind], "o")
    plt.scatter(x[0], x[1], c='r')
    a = -(w[1] / w[2])
    b = -(w[0] / w[2])
    t = [-3, 3]
    z = [a * t[0] + b, a * t[1] + b]
    plt.plot(t, z)
    plt.show()

def affiche_droite(title, x, w):
    plt.title(title)
    plt.scatter(x[0], x[1], c='r')
    a = -(w[1] / w[2])
    b = -(w[0] / w[2])
    t = [-3, 3]
    z = [a * t[0] + b, a * t[1] + b]
    plt.plot(t, z)
    plt.show()

#test multiPerceptron
#x = [1, 1]
#w1 = np.transpose([[-0.5, 2, -1], [0.5, 1, 0.5]])
#w2 = [2, -1, 1]
#yf, y1, y2 = multiPerceptron(x, w1, w2)
#print(yf)


# test apprentissage_widrow
x = np.array([[0, 1, 0, 1], [0, 0, 1, 1]])
yd = np.array([0, 1, 1, 0])
w1 = np.transpose([generateWeight(), generateWeight()])
w2 = generateWeight()
print("**************Les poids initiaux*****************")
print("les poids de neuron 1 de la couche caché : "+str(w1[:,0]))
print("les poids de neuron 2 de la couche caché: "+str(w1[:,1]))
print("les poids de neuron de la couche de sortie : "+str(w2))
w1, w2, errors = multiPerceptron_widrow(x, yd, w1, w2)
print("**************Apprentissage multicouches*****************")
print("Erreur final : "+str(errors[4999]))
print("les poids de neuron 1 de la couche caché : "+str(w1[:,0]))
print("les poids de neuron 2 de la couche caché: "+str(w1[:,1]))
print("les poids de neuron de la couche de sortie : "+str(w2))

print(multiPerceptron(x[:,0], w1, w2)[0])
print(multiPerceptron(x[:,1], w1, w2)[0])
print(multiPerceptron(x[:,2], w1, w2)[0])
print(multiPerceptron(x[:,3], w1, w2)[0])

plt.plot(errors)
plt.show()
#affiche_classe(x, yd, w1[:,0])
#affiche_classe(x, yd, w1[:,1])
#affiche_classe(x, yd, w2)