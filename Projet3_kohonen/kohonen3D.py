import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from pylab import *
from scipy.spatial import distance as dist
import matplotlib.image as mpimg


from scipy.io import loadmat

'''
Génere un indice d'individu au hasard
pram: n : la taille de l'ensemble d'apprentissage
'''
def generateRandomIndex(n):
    k = np.random.randint(0,n-1)
    return k

'''
Genere les poids aléatoires du début de l'algorithme d'apprentissage de taille kxkx2
'''
def generateWeights(k):
    weights = np.random.random(size=(k, k, k, 3))
    return weights

'''
Calcule la distance euclidienne d'un individu Xi au cluster du poid Wi
'''
def  distance(Xi, Wi):
    if len(Xi) != len(Wi):
        print('ERREUR : tailles DE Xi et Wi incompatibles !')
        return -1
    else:
        return dist.euclidean(Xi, Wi)

'''
Calcule le cluster gagnant (retourne ses coordonnées x et y)
param: Xi : l'individu choisi au hasard
param: W : l'ensemble des poids des noeuds
'''
def getWinnerCoordinates(Xi,W, k):
    if len(W) != k or len(W[0]) != k:
        print('ERREUR : W n\'est pas de taille KxKx2')
        return -1
    else:
        distances = []
        # Calculer dans cette boucle la distance de l'individu à chaque cluster
        for i in range(0, k):
            for j in range(k):
                for l in range(k):
                    diff = distance(Xi,W[i][j][l])
                    distances.append(diff)
        # Trier les indices des distances d'un ordre croissant
        sortedDistanceArgs = np.argsort(distances)
        # Calculer la projection de l'indice à la case 0 (correspondant à la distance min) sur les coordonnés x et y dans W
        xCoordinate = np.floor_divide(np.floor_divide(sortedDistanceArgs[0],k), k)
        yCoordinate = np.mod(np.floor_divide(sortedDistanceArgs[0],k),k)
        zCoordinate = np.mod(sortedDistanceArgs[0],k)
        # Retourner les coordonnés du cluster gagnant sous forme de vecteur à 2 dimensions.
        return np.array([xCoordinate, yCoordinate, zCoordinate])

'''
Calcule le paramètre de voisinage h(q,j)
'''
def getNeighbourParameter(winnerCoordinates,currentCoordinates,sigmaT, muT):
    if len(winnerCoordinates) != len(currentCoordinates) != 2:
        print('ERREUR : impossible de calculer da distance entre le cluster gagnant et le voisin faute de dimension !')
        return -1
    else:
        lateralDistance = dist.euclidean(winnerCoordinates,currentCoordinates)
        return muT * np.exp( -(lateralDistance / ( 2 * sigmaT**2 )) )

'''
Met à jous les poids des clusters
'''
def getUpdatedWeights(w , x , k , indIndex, winnerCoordinates , sigmaT , muT ):
    if len(w) != k or len(w[0]) != k:
        print('ERREUR : W n\'est pas de taille KxKx2')
        return -1
    else:
        for i in range(0, k):
            for j in range(0, k):
                for l in range (k):
                    w[i][j][l][0] = w[i][j][l][0] + getNeighbourParameter(winnerCoordinates, [i,j,l], sigmaT, muT) * (x[0][indIndex] - w[i][j][l][0])
                    w[i][j][l][1] = w[i][j][l][1] + getNeighbourParameter(winnerCoordinates, [i,j,l], sigmaT, muT) * (x[1][indIndex] - w[i][j][l][1])
                    w[i][j][l][1] = w[i][j][l][1] + getNeighbourParameter(winnerCoordinates, [i,j,l], sigmaT, muT) * (x[2][indIndex] - w[i][j][l][2])
        return  w
'''
Applique Kohonen sur 3 dimensions
'''
def kohonen3d(x,k,mu,sigma,nbiter):
    print('--------------------------------')
    print('Apprentissage KOHONEN 3D sur l\'image')
    print('--------------------------------')
    # Générer les poids aléatoires du début
    w = generateWeights(k)
    oldW = np.copy(w)
    # Nombre d'itération égale à nbiter ( à chaque iteration, un seul individu est présenté)
    for i in range(nbiter):
        print('> Apprentissage en cours ...  | Iteration :', i, ' / ',nbiter-1)
        # Mettre à jour les paramètres
        muT = mu[0] + (i / (nbiter - 1)) * (mu[-1] - mu[0])
        sigmaT = sigma[0] + (i / (nbiter - 1)) * (sigma[-1] - sigma[0])
        # Choisir l'indice d'un individu aléatoire
        indIndex = generateRandomIndex(len(x[0]))
        # Calculer les coordonnées du cluster gagnant
        winnerCoordinates = getWinnerCoordinates(x[:,indIndex],w,k)
        # Mettre à jour les poids
        w = getUpdatedWeights(w , x , k , indIndex, winnerCoordinates , sigmaT , muT )
    return w, oldW

def affiche_input(x):
    plt.plot(x[0], x[1])
    plt.show()
'''
Charger l'image sous forme d'une matrice [3, 256*256]
'''
def loadImage():
    print('--------------------------------')
    print('Chargement de l\'image')
    print('--------------------------------')
    img = mpimg.imread('samples/visage1.bmp')
    x = np.zeros(shape=(3, len(img) * len(img)))
    ind = 0
    for i in range(0, len(img)):
        for j in range(0, len(img)):
            x[:, ind] = img[i, j, :]
            ind += 1
    print('> Chargement de l\'image effectué.')
    return x,len(img)

'''
Codage de l'image : retourne la même matrice de l'image avec à chaque pixel, les coordonnées du neurone correspondant
'''
def encode(x, w):
    print('--------------------------------')
    print('Encodage de l\'image')
    print('--------------------------------')
    # Notre image est une matrice sous forme shape=(3,256*256), donc len(x[0]) permet d'avoire le nombre de pixels
    for i in range(len(x[0])):
        print('> Encodage en cours ...  | Pixel :', i, ' / ', len(x[0]) - 1)
        winnerCoordinates = getWinnerCoordinates(x[:,i], w, len(w[0]));
        # Mettre à la place de chaque pixel les coordonnées du neurone gagnant
        x[:,i] = winnerCoordinates
    return x

'''
Décodage de l'image : retourne une matrice d'image avec des couleurs réduites prête pour l'affichage
'''
def decode(x, w, imgSize):
    print('--------------------------------')
    print('Décodage de l\'image')
    print('--------------------------------')
    # Transformer chaque pixel en la couleur correspondante au poid du neurone associé
    for i in range(len(x[0])):
        print('> Décodage en cours ...  | Pixel :', i, ' / ', len(x[0]) - 1)
        # Les incides sont castés en entier car stockés dans x sous formes de réels
        x[:,i] = w[int(x[0,i]), int(x[1,i]), int(x[2,i])]

    # Faire une transformation de forme afin d'avoir une image sous forme d'une matrice valide pour l'affichage
    tmpImg = np.transpose(x)
    tmpImg = np.reshape(tmpImg, (imgSize, imgSize, 3))

    print('> Décodage deffectué.')
    return tmpImg

'''
afficher une image a partir d'une matrice
'''
def showImage(img,imgWithNoise):
    print('--------------------------------')
    print('Affichage de l\'image')
    print('--------------------------------')

    # Image d'origine
    fig1 = plt.figure()
    grid = fig1.add_gridspec(1, 1)
    original = mpimg.imread('samples/visage1.bmp')
    plt1 = fig1.add_subplot(grid[0,0])
    plt1.imshow(original.astype('uint8'))
    plt1.set_title('Image doriginale')

    # Image de couleurs réduites
    fig2 = plt.figure()
    grid = fig2.add_gridspec(1, 1)
    plt2 = fig2.add_subplot(grid[0,0])
    plt2.imshow(img.astype('uint8'))
    plt2.set_title('Image décodée après réduction de couleurs')

    # Image de couleurs réduites
    fig3 = plt.figure()
    grid = fig3.add_gridspec(1, 1)
    plt3 = fig3.add_subplot(grid[0, 0])
    plt3.imshow(imgWithNoise.astype('uint8'))
    plt3.set_title('Image décodée après réduction de couleurs et ajout de bruit')

    plt.show()

'''
Ajouter un bruit de valeur aléatoire sur chaqe pixel d'une image encodé encodedImage
'''
def getImageWithNoise(image):
    print('--------------------------------')
    print('Ajout de bruit')
    print('--------------------------------')
    tmp = image
    # Bruit appliqué à la deuxieme moitier de l'image
    for i in range( np.floor_divide(len(tmp[0]),2) , len(tmp[0]) ):
        noiseVector = np.random.random(size=(3))
        # print('-----------------------------', noiseVector)
        tmp[:,i] = noiseVector

    return tmp

'''
afficher une matrice 3d avec les axes x, y et z
'''
def plot3D(w, k):
    ax = plt.axes(projection='3d')
    
    # Data for three-dimensional scattered points
    x = np.zeros(shape=(3, k*k*k))

    ind = 0
    for i in range(k):
        for j in range(k):
            for l in range(k):
                x[:,ind] = np.copy(w[i][j][l])
                ind = ind+1
    ax.set_xlabel('Red')
    ax.set_ylabel('Green')
    ax.set_zlabel('Blue')
    cmhot = plt.get_cmap("hot")
    ax.scatter3D(x[0,:], x[1,:], x[2,:], c=x[2,:], cmap=cmhot);
###################################################### PROGRAMME PRINCIPAL

# Charger l'image
x, imgSize = loadImage()
# Apprentissage Kohonen
mu=np.array([0.5, 0.1])
sigma=np.array([3, 0.1])
w, oldW=kohonen3d(x,2,mu,sigma,20)
#affichage des poids en 3D
#plot3D(w, 8)

# Compresser l'image avec les poids de neurones obtenus de l'apprentissage
img = encode(x, w)
img1 = np.copy(img)

# Ajouter un bruit au dictionnaire encodé
encodedImageWithNoise = getImageWithNoise(img1)

# Réduire les couleurs en décodant l'image après ajout de bruit
# print(encodedImageWithNoise)
finalImageWithNoise = decode(encodedImageWithNoise, w, imgSize)

# Réduire les couleurs en décodant l'image en utilisant les poids des neurones
finalImage = decode(img, w, imgSize)

# Afficher l'image après ajout de bruit
showImage(finalImage, finalImageWithNoise)

'''
REMARQUE:
L'ajout d'un bruit léger sur l'image codée se voit immédiatement après décodage. 
Par contre, si l'on applique l'algorithme d'apprentissage sur une image originale 
puis rajouter un bruit léger sur la meme image avant son codage. 
la réduction de couleurs se fera sans un effet apparent après décodage vue que la couleur après le buit sera de toute façon liée au même 
neurone qu'avant le bruit.
'''