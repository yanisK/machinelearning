import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from pylab import *
from scipy.spatial import distance as dist
from scipy.io import loadmat

'''
Génere un indice d'individu au hasard
pram: n : la taille de l'ensemble d'apprentissage
'''
def generateRandomIndex(n):
    k = np.random.randint(0,n-1)
    return k

'''
Genere les poids aléatoires du début de l'algorithme d'apprentissage de taille kxkx2
'''
def generateWeights(k):
    weights = np.random.random(size=(k, k, 2))
    return weights

'''
Affiche les poids du reseau sous la forme d’une grille
'''
def affiche_grille(w):
    for b in range (0,w.shape[0]):
        plt.plot(w[b,:,0],w[b,:,1])

    for b in range (0,w.shape[1]):
        plt.plot(w[:,b,0],w[:,b,1])

    plt.show()

'''
Calcule la distance euclidienne d'un individu Xi au cluster du poid Wi
'''
def  distance(Xi, Wi):
    if len(Xi)!=len(Wi):
        print('ERREUR : tailles DE Xi et Wi incompatibles !')
        return -1
    else:
        return dist.euclidean(Xi, Wi)

'''
Calcule le cluster gagnant (retourne ses coordonnées x et y)
param: Xi : l'individu choisi au hasard
param: W : l'ensemble des poids des noeuds
'''
def getWinnerCoordinates(Xi,W, k):
    if len(W) != k or len(W[0]) != k:
        print('ERREUR : W n\'est pas de taille KxKx2')
        return -1
    else:
        distances = []
        # Calculer dans cette boucle la distance de l'individu à chaque cluster
        for i in range(0, k):
            for j in range(k):
                diff = distance(Xi,W[i][j])
                distances.append(diff)
        # Trier les indices des distances d'un ordre croissant
        sortedDistanceArgs = np.argsort(distances)
        # Calculer la projection de l'indice à la case 0 (correspondant à la distance min) sur les coordonnés x et y dans W
        xCoordinate = np.floor_divide(sortedDistanceArgs[0],k)
        yCoordinate = np.mod(sortedDistanceArgs[0],k)
        # Retourner les coordonnés du cluster gagnant sous forme de vecteur à 2 dimensions.
        return np.array([xCoordinate, yCoordinate])

'''
Calcule le paramètre de voisinage h(q,j)
'''
def getNeighbourParameter(winnerCoordinates,currentCoordinates,sigmaT, muT):
    if len(winnerCoordinates) != len(currentCoordinates) != 2:
        print('ERREUR : impossible de calculer da distance entre le cluster gagnant et le voisin faute de dimension !')
        return -1
    else:
        lateralDistance = distance(winnerCoordinates,currentCoordinates)
        return muT * np.exp( -(lateralDistance / ( 2 * np.power(sigmaT,2) )) )

'''
Met à jous les poids des clusters
'''
def getUpdatedWeights(w , x , k , indIndex, winnerCoordinates , sigmaT , muT ):
    if len(w) != k or len(w[0]) != k:
        print('ERREUR : W n\'est pas de taille KxKx2')
        return -1
    else:
        for i in range(0, k):
            for j in range(0, k):
                w[i][j][0] = w[i][j][0] + getNeighbourParameter(winnerCoordinates, [i,j], sigmaT, muT) *  (x[0][indIndex] - w[i][j][0])
                w[i][j][1] = w[i][j][1] + getNeighbourParameter(winnerCoordinates, [i,j], sigmaT, muT) * (x[1][indIndex] - w[i][j][1])
        return  w
'''
Applique Kohonen sur 2 dimensions
'''
def kohonen2d(x,k,mu,sigma,nbiter):
    # Générer les poids aléatoires du début
    w = generateWeights(k)
    # Nombre d'itération égale à nbiter ( à chaque iteration, un seul individu est présenté)
    for i in range(0,nbiter):
        # Mettre à jour les paramètres
        muT = mu[0] + (i / (nbiter - 1)) * (mu[-1] - mu[0])
        sigmaT = sigma[0] + (i / (nbiter - 1)) * (sigma[-1] - sigma[0])
        # Choisir l'indice d'un individu aléatoire
        indIndex = generateRandomIndex(len(x[0]))
        # Calculer les coordonnées du cluster gagnant
        winnerCoordinates = getWinnerCoordinates(x[:,indIndex],w,k)
        # Mettre à jour les poids
        w = getUpdatedWeights(w , x , k , indIndex, winnerCoordinates , sigmaT , muT )
    return w
'''
Pour afficher la distribution de l'ensemble d'apprentissage et affichage des poids du reseau sous la forme d’une grille
'''
def plotX(x,w):
    # X
    fig1 = plt.figure()
    plt1 = fig1.add_subplot(111)
    plt1.plot(x[0], x[1], 'ro--', linewidth=0.5)

    # W
    fig2 = plt.figure()
    plt2 = fig2.add_subplot(211)
    for b in range(0, w.shape[0]):
        plt2.plot(w[b, :, 0], w[b, :, 1])

    for b in range(0, w.shape[1]):
        plt2.plot(w[:, b, 0], w[:, b, 1])

    plt.show()
###################################################### PROGRAMME PRINCIPAL

Data = loadmat('samples/data.mat')
xdisc = Data['xdisc']
mu=np.array([0.5, 0.1])
sigma=np.array([3, 0.1])
w=kohonen2d(xdisc,8,mu,sigma,5000)
print(w)
# affiche_grille(w)
plotX(xdisc, w)